<?php

namespace App\Providers;

use App\Support\SHA1Hasher;
use function foo\func;
use Illuminate\Support\ServiceProvider;

class SHAHasherServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('hash',function(){
            return new SHA1Hasher();
        });
    }
}
