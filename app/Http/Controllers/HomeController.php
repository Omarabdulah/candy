<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function calculate(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                "money" => 'required|integer'
            ]);
            if($validator->fails())
                return response()->json(['results' => '<div class="alert alert-danger">'.$validator->messages()->first().'</div>']);
            else{
                $candies = floor($request->money / 5);
                $free_candies = $this->free_candies($candies);
                $total = $candies + $free_candies;

                return response()->json(['results' => view('result',compact('total','free_candies'))->render()]);
            }
        }catch (\Exception $e){

            return response()->json(['results' => '<div class="alert alert-danger"> Something Went wrong!</div>']);
        }
    }

    private function free_candies($candies){
        $free_candies = floor($candies / 3);
        if($candies >= 3)
            return $free_candies + $this->free_candies($free_candies);
        else
            $free_candies;
    }
}
