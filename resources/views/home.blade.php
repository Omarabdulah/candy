@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Calculator</div>
                <form method="POST" action="{{ route('calculate') }}" id="calculate">
                    {{csrf_field()}}
                <div class="form-group row">
                    <div class="col-md-4 text-right">
                        <h4>M :</h4>
                    </div>
                    <div class="col-md-8">
                        <input id="money" name="money" placeholder="Enter Amount" type="numbers" autofocus required>
                    </div>
                </div>
                </form>
                <div class="card-body text-center" id="results">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#money').on('keyup',function () {
                var input = $(this);
                var form = input.parents('form');
                $('#results').html('');
                if(input.val()) {
                    $.ajax({
                        method : form.attr('method'),
                        url: form.attr('action'),
                        data: {money: input.val()},
                        success: function (response) {
                            $('#results').html(response.results);
                        }
                    });
                }
            });
            $('#calculate').submit(function (e) {
                e.preventDefault();
            });
        });
    </script>
    @endsection
