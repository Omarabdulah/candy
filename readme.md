## Setup

- Clone the project
- Setup .env file or just copy from .env.example
- Create database "candy"
- run _composer update_
- run _php artisan migrate_
- run _php artisan db:seed_
- run server using _php artisan serv_ 

